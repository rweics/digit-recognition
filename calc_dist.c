/*
 * PROJ1-1: YOUR TASK A CODE HERE
 *
 * You MUST implement the calc_min_dist() function in this file.
 *
 * You do not need to implement/use the swap(), flip_horizontal(), transpose(), or rotate_ccw_90()
 * functions, but you may find them useful. Feel free to define additional helper functions.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "math.h"
#include "digit_rec.h"
#include "utils.h"
#include "limits.h"

unsigned int calc_dist_loop(unsigned char *image, int i_width, int i_height,
                            unsigned char *template, int t_width);
unsigned int calc_dist(unsigned char *image, unsigned char *template, int dim);
int get_index(int x, int y, int width);
unsigned char *crop(unsigned char *image, int img_w, int tgt_w, int x_offset,
                    int y_offset);
void swap(unsigned char *x, unsigned char *y);
void flip_horizontal(unsigned char *arr, int width);
void rotate_ccw_90(unsigned char *arr, int width);

/* Returns the minimum squared Euclidean distance between TEMPLATE and IMAGE. The size of
 * IMAGE is I_WIDTH * I_HEIGHT, while TEMPLATE is square with side length T_WIDTH. The template
 * image should be flipped, rotated, and translated across IMAGE.
 */
unsigned int calc_min_dist(unsigned char *image, int i_width, int i_height,
                           unsigned char *template, int t_width) {
  unsigned int min_dist = UINT_MAX, temp = 0;
  /* YOUR CODE HERE */

  // copy template content into mirror_template
  unsigned char mirror_template[t_width * t_width];
  unsigned char *pointer_template = template;
  for (int i = 0; i < t_width * t_width; i++) {
    mirror_template[i] = *(pointer_template);
    pointer_template++;
  }

  // rotating and iterating every possibility
  for (int operation = 0; operation < 8; operation++) {
    temp = calc_dist_loop(image, i_width, i_height, mirror_template, t_width);
    switch (operation) {
      case 3:
        flip_horizontal(mirror_template, t_width);
        break;
      default:
        rotate_ccw_90(mirror_template, t_width);
    }
    if (temp < min_dist) {
      min_dist = temp;
    }
  }

  return min_dist;
}

/*A loop to iterate the whole matrix, and return the smallest distance*/
unsigned int calc_dist_loop(unsigned char *image, int i_width, int i_height,
                            unsigned char *template, int t_width) {
  unsigned int min_dist = UINT_MAX;
  for (int i = 0; i < (i_width - t_width + 1); i++) {
    for (int j = 0; j < (i_height - t_width + 1); j++) {
      unsigned char *cropped = crop(image, i_width, t_width, i, j);
      unsigned int temp = calc_dist(cropped, template, t_width);
      if (temp < min_dist) {
        min_dist = temp;
      }
      free(cropped);
    }
  }
  return min_dist;
}

/* Returns the squared Euclidean distance between TEMPLATE and IMAGE. The size of
 * IMAGE is the same as TEMPLATE, which is square with side length T_WIDTH. (The IMAGE
 * is only one instance of translated section of the original IMAGE.
 */
unsigned int calc_dist(unsigned char *image, unsigned char *template, int dim) {
  unsigned int dist = 0;
  for (int i = 0; i < dim * dim; i++) {
    dist += (int) pow((double) (*image - *template), 2);
    image++;
    template++;
  }
  return dist;
}

/* Returns the index of 1D array while 2D X and Y coordinates are given. */
int get_index(int x, int y, int width) {
  return y * width + x;
}

/* Crops a rectangular IMAGE with width IMG_W into a square image with width TGT_W */
unsigned char *crop(unsigned char *image, int img_w, int tgt_w, int x_offset,
                    int y_offset) {
  unsigned char *cropped = malloc(tgt_w * tgt_w * sizeof(unsigned char));  // need failure handling
  if (!cropped) {
    allocation_failed();
  }
  int x = 0, y = 0;
  for (y = 0; y < tgt_w; y++) {
    for (x = 0; x < tgt_w; x++) {
      *(cropped + get_index(x, y, tgt_w)) = *(image
          + get_index(x + x_offset, y + y_offset, img_w));
    }
  }
  return cropped;
}

/* Swaps the values pointed to by the pointers X and Y. */
void swap(unsigned char *x, unsigned char *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

/* Flips the elements of a square array ARR across the y-axis. */
void flip_horizontal(unsigned char *arr, int dim) {
  int x, y;
  for (y = 0; y < dim * dim; y += dim) {
    for (x = 0; x < dim / 2; x += 1) {
      swap(arr + x + y, arr + dim - x - 1 + y);
    }
  }
}

/* Rotates the square array ARR by 90 degrees clockwise. */
void rotate_ccw_90(unsigned char *arr, int width) {
  unsigned char temp[width * width];
  int counter = 0;
  for (int x = 0; x < width; x++) {
    for (int y = 0; y < width; y++) {
      int index = get_index(x, (width - y - 1), width);
      int p = arr[index];
      temp[counter] = p;
      counter++;
    }
  }
  for (int i = 0; i < width * width; i++) {
    arr[i] = temp[i];
  }
}
